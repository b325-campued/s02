-- This is a comment: SQL syntaxes are not case sensitive (but still follow consistency for best practice)
-- If you are going to a add a sql syntax(capitalized)
-- If you are going to use or add name of column or table(small letter)

-- TO show all thje list of our databases
-- SQL is strict also to ; as end of statement
SHOW DATABASES;

-- TO create/add a database
-- CREATE DATABASE <name_of_db>;
CREATE DATABASE music_db;

-- TO drop/delete a database
-- DROP DATABASE <name_of_db>;
DROP DATABASE trial_db;

-- TO select a database
-- USE <name_of_db>;
USE music_db;

-- TO show all the list of our tables
-- Make sure there's selected database
SHOW TABLES;

-- TO create/add a table in a database
-- Create tables with no foreign key first
-- Table name is plural
-- Unlike MongoDB we need to create a id here 
-- CREATE TABLE <name_of_table>();
-- SQL data types : https://blog.devart.com/mysql-data-types.html
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)

);

-- artist_id is singular here since 1 value per cell
CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(20) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT

);

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_title VARCHAR(50) NOT NULL,
	song_length TIME NOT NULL,
	song_genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_albums_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	date_created DATETIME NOT NULL,
	user_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlist_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
	FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,

	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);



-- Mini Activity
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- renaming a table
-- RENAME TABLE <table_name_to_rename> TO <new_table_name>;
-- ALTER TABLE <table_name_to_rename> RENAME <new_table_name>;


